sahara-dashboard (19.0.0-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090691).

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Dec 2024 10:02:26 +0100

sahara-dashboard (19.0.0-2) unstable; urgency=medium

  * Removed build-depends on python3-unittest2 (Closes: #1060634).

 -- Thomas Goirand <zigo@debian.org>  Mon, 11 Mar 2024 09:30:24 +0100

sahara-dashboard (19.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Oct 2023 12:10:07 +0200

sahara-dashboard (19.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Cleans better.

 -- Thomas Goirand <zigo@debian.org>  Sun, 17 Sep 2023 11:09:36 +0200

sahara-dashboard (18.0.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1045786).

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Aug 2023 17:28:02 +0200

sahara-dashboard (18.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 16:45:59 +0200

sahara-dashboard (18.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Mar 2023 16:06:20 +0100

sahara-dashboard (18.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Switch to debhelper 11.
  * Standards-version bump to 4.6.1.
  * Removed django-4-force_text-is-removed.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Mar 2023 09:28:02 +0100

sahara-dashboard (17.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2022 10:44:03 +0200

sahara-dashboard (17.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed 2 patches applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Sep 2022 10:11:09 +0200

sahara-dashboard (16.0.0-2) unstable; urgency=medium

  * Add Django 4 compat patches (Closes: #1015078):
    - django-4-ugettext_lazy-is-removed.patch
    - django-4-django.conf.urls.url-is-removed.patch
    - debian/patches/django-4-force_text-is-removed.patch
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Sun, 31 Jul 2022 12:24:53 +0200

sahara-dashboard (16.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 22:49:06 +0200

sahara-dashboard (16.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Mar 2022 12:42:43 +0200

sahara-dashboard (16.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 12 Mar 2022 20:48:15 +0100

sahara-dashboard (15.0.0-1) unstable; urgency=medium

  * New upstream releasse.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 17:33:57 +0200

sahara-dashboard (15.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 13:55:23 +0200

sahara-dashboard (15.0.0~rc1-1) experimental; urgency=medium

  * new upstream release.
  * (Build-)depends on minimum horizon >= 20.0.0+git2020.09.21.27036cc0eb.

 -- Thomas Goirand <zigo@debian.org>  Tue, 21 Sep 2021 14:49:10 +0200

sahara-dashboard (14.0.0-4) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 16:32:21 +0200

sahara-dashboard (14.0.0-3) experimental; urgency=medium

  * Add rm_conffile to remove old files in /etc/openstack-dashboard/enable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 May 2021 12:42:14 +0200

sahara-dashboard (14.0.0-2) experimental; urgency=medium

  * Package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled.
  * Add Breaks: python3-django-horizon (<< 3:19.2.0-2~).

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 May 2021 16:36:30 +0200

sahara-dashboard (14.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Apr 2021 16:51:38 +0200

sahara-dashboard (14.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Mar 2021 15:18:31 +0100

sahara-dashboard (13.0.0-3) unstable; urgency=medium

  * Fix wrong version of python3-django-horizon break.

 -- Thomas Goirand <zigo@debian.org>  Mon, 05 Jul 2021 09:50:24 +0200

sahara-dashboard (13.0.0-2) unstable; urgency=medium

  * Remove Breaks+Replaces: python-sahara-dashboard (as it's not in Stable).
  * Package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled.

 -- Thomas Goirand <zigo@debian.org>  Thu, 01 Jul 2021 12:12:48 +0200

sahara-dashboard (13.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2020 17:00:12 +0200

sahara-dashboard (13.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Sep 2020 08:51:58 +0200

sahara-dashboard (12.0.0-1) unstable; urgency=medium

  * Fixed Homepage URL.
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 May 2020 19:15:12 +0200

sahara-dashboard (12.0.0~rc2-2) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 09 May 2020 21:29:40 +0200

sahara-dashboard (12.0.0~rc1-1) experimental; urgency=medium

  * Move the package to horizon-plugins subgroup on Salsa.
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 24 Apr 2020 13:31:16 +0200

sahara-dashboard (11.0.0-2) unstable; urgency=medium

  * d/watch: Fix watch file

 -- Michal Arbet <michal.arbet@ultimum.io>  Fri, 13 Mar 2020 15:46:48 +0100

sahara-dashboard (11.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Oct 2019 21:17:00 +0200

sahara-dashboard (11.0.0~rc1-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 11 Oct 2019 09:11:13 +0200

sahara-dashboard (10.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Michal Arbet ]
  * Do not move files from source, copy it,
    kolla deployment expects that files are in /usr/lib...

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 22 Aug 2019 12:12:24 +0200

sahara-dashboard (10.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Apr 2019 12:45:07 +0200

sahara-dashboard (10.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed package versions when satisfied in Buster.
  * Fixed min version of saharaclient.

 -- Thomas Goirand <zigo@debian.org>  Sun, 31 Mar 2019 23:34:56 +0200

sahara-dashboard (9.0.1-1) unstable; urgency=medium

  * New upstream version
  * Redesign sahara-dashboard:
      - Enabled files now in /etc/openstack-dashboard/
      - Removed post scripts which is now achieved by a trigger
  * d/copyright: Update copyright
  * d/control: Add me to uploaders field

 -- Michal Arbet <michal.arbet@ultimum.io>  Mon, 21 Jan 2019 21:35:20 +0100

sahara-dashboard (9.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Relax python3-django lower version.
  * Test only in sahara_dashboard/test.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Sep 2018 20:45:41 +0200

sahara-dashboard (9.0.0~rc2-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 28 Aug 2018 16:48:15 +0200

sahara-dashboard (8.0.0-1) unstable; urgency=medium

  [ Thomas Goirand ]
  * Added missing EPOC in (build-)depends: openstack-dashboard.
  * New upstream release:
    - Doesn't depend on django-openstack-auth (Closes: #893245).
  * Fixed (build-)depends for this release.
  * Switched to Python 3.
  * Removed all patches applied upstream.

  [ Ondřej Nový ]
  * d/rules: Removed UPSTREAM_GIT with default value
  * d/control: Set Vcs-* to salsa.debian.org

  [ Daniel Baumann ]
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Deprecating priority extra as per policy 4.0.1.

 -- Thomas Goirand <zigo@debian.org>  Mon, 12 Mar 2018 14:28:25 +0100

sahara-dashboard (5.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2016 18:23:10 +0200

sahara-dashboard (5.0.0~rc2-2) unstable; urgency=medium

  * Uploading to unstable (Closes: #837222).

 -- Thomas Goirand <zigo@debian.org>  Tue, 04 Oct 2016 15:19:09 +0200

sahara-dashboard (5.0.0~rc2-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/rules: Removed UPSTREAM_GIT with default value

  [ Thomas Goirand ]
  * Added missing EPOC in (build-)depends: openstack-dashboard.
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed patch applied upstream:
    - request.REQUEST-is-removed.patch
    - remove-load-url-from-future.patch
    - disable-failed-manila-test.patch.
  * Using OpenStack's Gerrit as VCS URLs:
    - Point .gitreview file to packaging-deb project.
    - Add .gitreview to extend-diff-ignore in d/s/options.

 -- Thomas Goirand <zigo@debian.org>  Wed, 20 Jul 2016 09:24:22 +0200

sahara-dashboard (4.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)

  * Adds -f flag when deleting .secret_key_store file.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 Apr 2016 22:36:56 +0000

sahara-dashboard (4.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Thu, 07 Apr 2016 22:34:21 +0200

sahara-dashboard (4.0.0~rc2-1) unstable; urgency=medium

  * Initial release. (Closes: #812836)

 -- Thomas Goirand <zigo@debian.org>  Mon, 25 Jan 2016 21:39:26 +0800
